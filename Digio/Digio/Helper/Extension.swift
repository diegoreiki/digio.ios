import Foundation
import UIKit

extension UIViewController{
    
    func converterImage(url: String) -> Data?{
        let imageUrl = URL(string: url)
        let imageData = NSData(contentsOf: imageUrl!)
        return imageData as Data?
    }
}
