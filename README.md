**Digio iOS Developer**

O App consiste em realizar a requisição no endpoint hospedagem em https://7hgi9vtkdc.execute-api.sa-east-1.amazonaws.com/sandbox/products para leitura dos dados e renderização nos componentes do app para iOS.

---

## Instalação

Para executar o projeto em modo de desenvolvimento, é necessário ter o Cocoapods instalado. Abra o terminal e rode os comandos abaixo:

sudo gem install cocoapods (para instalar o cocoapods caso ainda não tenha)
pod install (para instalar as dependências do projeto)
abrir no Xcode o arquivo GrupoZap.xcworkspace

1. sudo gem install cocoapods (para instalar o cocoapods caso ainda não tenha)
2. pod install (para instalar as dependências do projeto)
3. abrir no Xcode o arquivo **Digio.xcworkspace**

---

## Bibliotecas
Bibliotecas utilizadas via cocoapods

1. Alamofire

---

## Linguagem
1. Swift 5

---

## IDE
1. Xcode 12.2 

---

## Autor
1. Diego Silva Costa Pereira
2. diegoasp@gmail.com

---

