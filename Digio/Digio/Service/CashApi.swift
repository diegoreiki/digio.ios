import UIKit
import Alamofire

class CashApi: NSObject {
    func requestBannerCash(returnCash: @escaping ((_ cash: Cash) -> Void), occurredError: ((_ error: NSError?) -> Void)?){
        
        let url = Constants.base_url
    
        AF.request(url, method: .get, parameters: nil).validate().responseJSON { (response) in
            switch response.result{
            case .success:
                if let JSON = response.value as? [String: Any]{

                    guard let cashJSON = JSON["cash"] as? [String: Any] else { return }
                    guard let jsonData = Cash.converterJSON(cashJSON) else { return }
                    guard let cashObject = Cash.decodableGenres(jsonData) else { return }

                    returnCash(cashObject)
                }
            case .failure(let error):
                if occurredError != nil {
                    occurredError!(error as NSError)
                }
                break
            }
        }
    }
}
