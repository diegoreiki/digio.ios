import UIKit

class HomeViewController: UIViewController {
    
    //MARK: Property
    var spotlight = [Spotlight]()
    var products = [Product]()
    var descriptionCash: String?
    
    @IBOutlet weak var collectionViewSpotlight: UICollectionView!
    @IBOutlet weak var collectionViewProcuts: UICollectionView!
    @IBOutlet weak var imageCash: UIImageView!
    
    //MARK: Lifecicle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getSpotlights()
        getCashBanner()
        getAllProducts()
        
        Radius().views([imageCash], radius: 10, maskToBound: true)
    }
    
    //MARK: Methods
    func getSpotlights(){
        if statusInternet() {
            SpotlightApi().requestSpotlight { (spotlight) in
                self.spotlight = spotlight
                self.collectionViewSpotlight.reloadData()
            } occurredError: { (error) in
                Alert(controller: self).show(title: Constants.title.error, message: Constants.message.error, titleButtonAction: "Ok", titleButtonCancel: nil, completion: nil)
            }
        } else {
            Alert(controller: self).show(title: Constants.title.error, message: Constants.message.error_no_internet, titleButtonAction: "Ok", titleButtonCancel: nil, completion: nil)
        }
    }
    
    func getCashBanner(){
        if statusInternet() {
            CashApi().requestBannerCash { (cash) in
                self.descriptionCash = cash.description_banner
                
                let imageBanner = cash.bannerURL
                if let imageData = self.converterImage(url: imageBanner){
                    self.imageCash.image = UIImage(data: imageData)
                    self.imageCash.layer.masksToBounds = true
                    self.imageCash.contentMode = .scaleAspectFill
                }
            } occurredError: { (error) in
                Alert(controller: self).show(title: Constants.title.error, message: Constants.message.error, titleButtonAction: "Ok", titleButtonCancel: nil, completion: nil)
            }
        } else {
            Alert(controller: self).show(title: Constants.title.error, message: Constants.message.error_no_internet, titleButtonAction: "Ok", titleButtonCancel: nil, completion: nil)
        }
    }
    
    func getAllProducts(){
        if statusInternet() {
            ProductsApi().requestProducts { (products) in
                self.products = products
                self.collectionViewProcuts.reloadData()
            } occurredError: { (error) in
                Alert(controller: self).show(title: Constants.title.error, message: Constants.message.error, titleButtonAction: "Ok", titleButtonCancel: nil, completion: nil)
            }
        } else {
            Alert(controller: self).show(title: Constants.title.error, message: Constants.message.error_no_internet, titleButtonAction: "Ok", titleButtonCancel: nil, completion: nil)
        }
    }
    
    func statusInternet() -> Bool {
        return AlamofireHelper.shareInstance.checkConnection() 
    }
}

extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.collectionViewSpotlight {
            return spotlight.count
        } else {
            return products.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.collectionViewSpotlight {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "spotlight_cell", for: indexPath) as! SpotlightCollectionViewCell
            
            if let imageData = converterImage(url: self.spotlight[indexPath.row].bannerURL){
                cell.imageSpotlight.image = UIImage(data: imageData)
                cell.imageSpotlight.layer.masksToBounds = true
                cell.imageSpotlight.contentMode = .scaleAspectFill
            }
            return cell;
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "product_cell", for: indexPath) as! ProductCollectionViewCell
            
            if let imageData = converterImage(url: self.products[indexPath.row].imageURL){
                cell.imageProduct.image = UIImage(data: imageData)
                cell.imageProduct.layer.masksToBounds = true
                cell.imageProduct.contentMode = .scaleToFill
            }
            return cell;
        }
    }
        
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        if collectionView == self.collectionViewSpotlight {
            return CGSize(width: 300.0, height: 141.0)
        } else {
            return CGSize(width: 120.0, height: 120.0)
        }
    }
}
