import UIKit

class Spotlight: NSObject, Decodable {

    //MARK: Property
    let name: String
    let bannerURL: String
    let description_banner: String
    
    enum CodingKeys: String, CodingKey {
        case name
        case bannerURL
        case description_banner = "description"
    }
    
    //MARK: Init
    init(name: String, banner: String, description: String) {
        self.name = name
        self.bannerURL = banner
        self.description_banner = description
    }
    
    //MARK: Methods
    class func converterJSON(_ json: [[String: Any]]) -> Data?{
        return try? JSONSerialization.data(withJSONObject: json, options: [])
    }
    
    class func decodableGenres(_ jsonData: Data) -> [Spotlight]?{
        do {
            return try JSONDecoder().decode([Spotlight].self, from: jsonData)
        } catch {
            print(error.localizedDescription)
            return nil
        }
    }
}
