import UIKit

class SpotlightCollectionViewCell: UICollectionViewCell {
    
    //MARK: Property
    @IBOutlet weak var imageSpotlight: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        Radius().views([imageSpotlight], radius: 10, maskToBound: true)
    }
}
