import UIKit
import Alamofire

class AlamofireHelper: NSObject {
    
    static let shareInstance = AlamofireHelper()
    
    func checkConnection() -> Bool{
        return NetworkReachabilityManager()!.isReachable
    }
}
