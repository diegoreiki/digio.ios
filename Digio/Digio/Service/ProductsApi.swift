import UIKit
import Alamofire

class ProductsApi: NSObject {
    func requestProducts(returnProducts: @escaping ((_ products: [Product]) -> Void), occurredError: ((_ error: NSError?) -> Void)?){
        
        let url = Constants.base_url
    
        AF.request(url, method: .get, parameters: nil).validate().responseJSON { (response) in
            switch response.result{
            case .success:
                if let JSON = response.value as? [String: Any]{

                    guard let productsJSON = JSON["products"] as? [[String: Any]] else { return }
                    guard let jsonData = Spotlight.converterJSON(productsJSON) else { return }
                    guard let productsObject = Product.decodableProducts(jsonData) else { return }

                    returnProducts(productsObject)
                }
            case .failure(let error):
                if occurredError != nil {
                    occurredError!(error as NSError)
                }
                break
            }
        }
    }
}
