import Foundation

struct Constants {
    
    static var base_url = "https://7hgi9vtkdc.execute-api.sa-east-1.amazonaws.com/sandbox/products"
    
    enum title {
        static var error = "Erro"
        static var warning = "Atenção"
    }
    
    enum message {
        static var error = "Ocorreu um error desconhecido"
        static var error_no_internet = "Verifique sua internet e tente novamente"
    }
}
