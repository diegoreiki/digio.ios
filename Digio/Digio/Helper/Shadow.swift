import UIKit

class Shadow: NSObject {

    func views(_ views: [UIView], opacity: Float, radius: Float, offsetWidth: Float, offsetHeight: Float) {
        for view in views{
            view.layer.shadowOpacity = opacity
            view.layer.shadowRadius = CGFloat(radius)
            view.layer.shadowColor = UIColor.gray.cgColor
            view.layer.shadowOffset = CGSize(width: CGFloat(offsetWidth), height: CGFloat(offsetHeight))
        }
    }
}
