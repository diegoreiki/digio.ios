import UIKit

class ProductCollectionViewCell: UICollectionViewCell {
    
    //MARK: Property
    @IBOutlet weak var viewProduct: UIView!
    @IBOutlet weak var imageProduct: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        Radius().views([viewProduct], radius: 10, maskToBound: true)
        Shadow().views([viewProduct], opacity: 1, radius: 10, offsetWidth: 2, offsetHeight: 2)
    }
}
