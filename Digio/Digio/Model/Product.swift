import UIKit

class Product: NSObject, Decodable {

    //MARK: Property
    let name: String
    let imageURL: String
    let description_product: String
    
    enum CodingKeys: String, CodingKey {
        case name
        case imageURL
        case description_product = "description"
    }
    
    //MARK: Init
    init(name: String, image: String, description: String) {
        self.name = name
        self.imageURL = image
        self.description_product = description
    }
    
    //MARK: Methods
    class func converterJSON(_ json: [[String: Any]]) -> Data?{
        return try? JSONSerialization.data(withJSONObject: json, options: [])
    }
    
    class func decodableProducts(_ jsonData: Data) -> [Product]?{
        do {
            return try JSONDecoder().decode([Product].self, from: jsonData)
        } catch {
            print(error.localizedDescription)
            return nil
        }
    }
}
