import UIKit

class Radius: NSObject {

    func images(_ images: [UIImageView], radius: Float, maskEnable: Bool) {
        for image in images{
            image.layer.cornerRadius = CGFloat(radius)
            image.layer.masksToBounds = maskEnable
        }
    }
    
    func views(_ views: [UIView], radius: Float, maskToBound: Bool){
        for view in views{
            view.layer.cornerRadius = CGFloat(radius)
            view.layer.masksToBounds = maskToBound
        }
    }
    
    func buttons(_ buttons: [UIButton], radius: Float, maskToBounds: Bool){
        for button in buttons{
            button.layer.cornerRadius = CGFloat(radius)
            button.layer.masksToBounds = maskToBounds
        }
    }
    
    func textView(_ textViews: [UITextView], radius: Float, maskToBounds: Bool){
        for textView in textViews{
            textView.layer.cornerRadius = CGFloat(radius)
            textView.layer.masksToBounds = maskToBounds
        }
    }
}
