import UIKit
import Alamofire

class SpotlightApi: NSObject {

    func requestSpotlight(returnSpotight: @escaping ((_ spotlight: [Spotlight]) -> Void), occurredError: ((_ error: NSError?) -> Void)?){
        
        let url = Constants.base_url
    
        AF.request(url, method: .get, parameters: nil).validate().responseJSON { (response) in
            switch response.result{
            case .success:
                if let JSON = response.value as? [String: Any]{

                    guard let spotlightJSON = JSON["spotlight"] as? [[String: Any]] else { return }
                    guard let jsonData = Spotlight.converterJSON(spotlightJSON) else { return }
                    guard let spotlightObject = Spotlight.decodableGenres(jsonData) else { return }

                    returnSpotight(spotlightObject)
                }
            case .failure(let error):
                if occurredError != nil {
                    occurredError!(error as NSError)
                }
                break
            }
        }
    }
}
