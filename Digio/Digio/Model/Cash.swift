import UIKit

class Cash: NSObject, Decodable {

    //MARK: Property
    let title: String
    let bannerURL: String
    let description_banner: String
    
    enum CodingKeys: String, CodingKey {
        case title
        case bannerURL
        case description_banner = "description"
    }
    
    //MARK: Init
    init(title: String, banner: String, description: String) {
        self.title = title
        self.bannerURL = banner
        self.description_banner = description
    }
    
    //MARK: Methods
    class func converterJSON(_ json: [String: Any]) -> Data?{
        return try? JSONSerialization.data(withJSONObject: json, options: [])
    }
    
    class func decodableGenres(_ jsonData: Data) ->Cash?{
        do {
            return try JSONDecoder().decode(Cash.self, from: jsonData)
        } catch {
            print(error.localizedDescription)
            return nil
        }
    }
}
